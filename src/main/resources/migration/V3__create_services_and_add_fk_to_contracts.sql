
create table services(
    id int not null,
    type varchar (128),
    primary key (id)
);

alter table contracts add service_id int;
alter table contracts add foreign key (service_id) references services(id);

