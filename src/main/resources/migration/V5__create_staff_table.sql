
create table staff(
    id int not null,
    first_name varchar (128),
    last_name varchar (128),
    office_id int,
    primary key (id),
    foreign key (office_id) references offices(id)
)
