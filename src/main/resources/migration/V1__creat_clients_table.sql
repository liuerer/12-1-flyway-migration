
create table clients(
    id int not null,
    full_name varchar(128),
    abbreviation varchar(6),
    primary key (id)
)
